package com.geektechnique.serviceregdiscovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceRegDiscoveryApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceRegDiscoveryApplication.class, args);
    }

}
